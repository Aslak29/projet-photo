<?php

namespace App\Models;

use CodeIgniter\Model;

class Mcompetition extends Model
{
    protected $table = 'competition';
    protected $primaryKey = 'ID';
    protected $returnType = 'array';

    //fonction pour tout afficher
    public function getAll()
    {
        $requete = $this->select('ID, Nom, DossierStockage, Date');
        return $requete->findAll();
    }

    public function select_detail_by_id($prmidCompet){
        $requete = $this->select('ID, Nom, DossierStockage, Date')
        ->where(['ID'=>$prmidCompet]);
        return $requete->findAll();
    }


}
