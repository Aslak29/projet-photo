<?php

namespace App\Controllers;
use App\Models\Mcompetition;
use App\Models\Mphoto;
use CodeIgniter\Exceptions\PageNotFoundException;
use CodeIgniter\Controller;

class Ccompetition extends Controller
{
    public function index()
    {
        $model = new Mcompetition();
        $data['result'] = $model->getAll();

        $data['page_title'] = "La liste des compétitions";
        $data['titre1'] = "La liste des compétitions";


        $page['contenu'] = view('Competition/v_liste_competition', $data);
        return view('Commun/v_template', $page);
    }

    public function detail($prmidCompet = null)
    {

        if ($prmidCompet != null) {
            $model = new Mcompetition();
            $data['result'] = $model->select_detail_by_id($prmidCompet);
            if (($data['result']) != 0) {
                $data['page_title'] = "La compétition Nature ";
                $data['titre1'] = "La compétition" . $prmidCompet;

                $page['contenu'] = view('Competition/v_detail_competition', $data);
                return view('Commun/v_template', $page);
            } else {
                throw PageNotFoundException::forPageNotFound("Cette compétition n'existe pas !");
            }
        } else {
            throw PageNotFoundException::forPageNotFound("Il faut choisir une compétition");
        }
    }

   
}