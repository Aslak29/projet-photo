<?php

namespace App\Controllers;
use App\Models\Mcompetition;
use App\Models\Mphoto;
use CodeIgniter\Exceptions\PageNotFoundException;
use CodeIgniter\Controller;

class Ccompetition extends Controller
{
    public function index()
    {
        $model = new Mphoto();
        $data['result'] = $model->getAll();

        $data['page_title'] = "La liste des compétitions";
        $data['titre1'] = "La liste des compétitions";


        $page['contenu'] = view('Competition/v_liste_competition', $data);
        return view('Commun/v_template', $page);
    }
}