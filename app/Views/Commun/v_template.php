<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $page_title; ?></title>
    <link href="<?php echo base_url("css/styles.css") ?>" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="conteneur">
        <header>
            <h1>Concours Photographique</h1>
        </header>

        <nav>
            <ul>

                <li><a href="<?php echo base_url("Caccueil"); ?>">Accueil</a></li>
                <li><a href="<?php echo base_url("Ccompetition"); ?>">Les compétitions</a></li>
                <li><a href="<?php echo base_url("Cgalerie"); ?>">Galerie Photo</a></li>

            </ul>
        </nav>

        <section>
            <?php echo $contenu; ?>

        </section>

        <footer>
            <p>Copyright - Tous droits réservés -
                <a href="#">Contact</a>
            </p>
        </footer>
    </div>
</body>

</html>